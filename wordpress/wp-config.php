<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_vsp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`wpu>r{/%Vk>|cnQ9LEE=gDm9Ew=jrAh=DL!wA`7>d}UXa@2sL][G/JHwI>(;zo;');
define('SECURE_AUTH_KEY',  'ff<w||3tPDW +iAUZA.9cj2vO),]sZ0wM$C-(wp-QZ)vLElkGST0/V=gDvoTaT,l');
define('LOGGED_IN_KEY',    'hQ<?lQ!FWP+z[@)|8TulkT&4,-*qs`Vl_oOF{@L(M?^0%@v0#h!xwjbj;hCQlbaW');
define('NONCE_KEY',        '~U4eJ?Nsq[]xKZ)RP!GNp/G1x*n8Ag=28l+)i5;CIu;V7??2OHO-l<;$=7,<>5f7');
define('AUTH_SALT',        'Xm=vLGO@v!1VrB|-N4a!K$As_nZ,XgPmZ`^Sx[:ZJAO<0|uSEgjP-||HZ?Fd;b<u');
define('SECURE_AUTH_SALT', 'mU9da5+@1U-1PE_M7pnOs=fO|x44)=(hFU80$1qRO<t_D9M $sY4++x|:E!%o+(U');
define('LOGGED_IN_SALT',   'yva6=3:Oas<AA WdD.^v4}OV}];=?Ne8ZCcaT-,Ud Qn>CkCO++hP:7USbnz=<&Y');
define('NONCE_SALT',       'D:n*hpCTd*M{+XNX!:]-|3k?|y%@bS{!AFe1dPIB+Ua4?]<59H8/]krmF,g:1>Ui');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
